from chalice import Chalice
import boto3
import json

app = Chalice(app_name='wordgames-lambda')


@app.route('/')
def index():
    return { 'status': 'ok' }

@app.route('/word/{w}')
def words(w):
  drow = w.lower()
  lendrow = len(drow)
  ldrow = list(drow)
  ldrow.sort()
  s3 = boto3.client('s3')
  obj = s3.get_object(Bucket = "wordgames-lambda", Key = "words")
  ordw = []
  for line in obj['Body'].read().splitlines():
      word = line.decode('utf-8')
      lenword = len(word)
      if(lendrow >= lenword):
          word = word.lower()
          lword = list(word)
          lword.sort()
          i = 0
          j = 0
          matched = False
          while (i < lenword and j < lendrow):
              if(ldrow[j] != lword[i]):
                  matched = False
                  j += 1
              else:
                  matched = True
                  i += 1
                  j += 1
                  if(j == lendrow and i < lenword):
                      matched = False
          #return { 'word': word, 'lword': lword, 'lendrow': lendrow, 'lenword': lenword }
          if(matched):
          #print(i,j,ldrow, lword)
               ordw.append({ 'match': word.upper() })
          #if(matched == 1):
          #print(i,j,ldrow, lword)
  return ordw

# @app.route('/users', methods=['POST'])
# def create_user():
#     # This is the JSON body the user sent in their POST request.
#     user_as_json = app.current_request.json_body
#     # We'll echo the json body back to the user in a 'user' key.
#     return {'user': user_as_json}


