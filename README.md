## Wordgames Lambda

- Install pip e.g. `sudo apt install python-pip`
- Install chalice e.g. `pip install chalice`
- Install awscli e.g. `sudo apt install awscli`
- Configure Access Key Id and Secret e.g. `aws configure`
- Deploy e.g. `chalice deploy`

You can add `--stage prod` to `chalice deploy` if you are ready for production.

Sample deployment [here](https://85066bjki1.execute-api.us-east-1.amazonaws.com/api/word/anseet)

